from django.shortcuts import render
from django.views.generic.edit import FormView
from .forms import RegisterForm, LoginForm


def index(request):
    return render(request, 'index.html', {'email': request.session.get('user')})


class RegisterView(FormView):
    # 요청받은 회원가입 페이지로 랜더링 하면 form 을 함께 넘긴다.
    template_name = 'user/register.html'
    # forms 에 있는 폼을 뷰에 등록여 것
    form_class = RegisterForm
    success_url = '/'


class LoginView(FormView):
    template_name = 'user/login.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        self.request.session['user'] = form.email

        return super().form_valid(form)