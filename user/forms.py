from django import forms
from .models import User
from django.contrib.auth.hashers import make_password, check_password


class RegisterForm(forms.Form):
    # 템플릿에서 받아올 data 형식
    email = forms.EmailField(
        error_messages={
            'required': '이메일을 입력하세요!'
        }, max_length=64, label='이메일'
    )
    password = forms.CharField(
        error_messages={
            'required': '비밀번호를 입력하세요!'
        }, widget=forms.PasswordInput, label='비밀번호'
    )
    re_password = forms.CharField(
        error_messages={
            'required': '비밀번호를 입력하세요!'
        }, widget=forms.PasswordInput, label='비밀번호 확인'
    )

    # 받은 데이터 유효성 검사
    def clean(self):
        # 템플릿에서 받아온 데이터를 딕셔너리로 할당
        cleaned_data = super().clean()
        # 딕셔너리 데이터를 키별로 할당한다
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        re_password = cleaned_data.get('re_password')

        if password and re_password:
            if password != re_password:
                self.add_error('password', '비밀번호가 다릅니다.')
                self.add_error('re_password', '비밀번호가 다릅니다.')
            else:
                # db에 저장할 사용자 인스턴스 생성
                user = User(
                    email=email,
                    password=make_password(password)
                )
                # db에 저장하기
                user.save()


class LoginForm(forms.Form):
    # 템플릿에서 받아올 data 형식
    email = forms.EmailField(
        error_messages={
            'required': '이메일을 입력하세요!'
        }, max_length=64, label='이메일'
    )
    password = forms.CharField(
        error_messages={
            'required': '비밀번호를 입력하세요!'
        }, widget=forms.PasswordInput, label='비밀번호'
    )

    # 받은 데이터 유효성 검사
    def clean(self):
        # 템플릿에서 받아온 데이터를 딕셔너리로 할당
        cleaned_data = super().clean()
        # 딕셔너리 데이터를 키별로 할당한다
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        if email and password:
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                self.add_error('email', '이메일이 없습니다.')
                return

            if not check_password(password, user.password):
                self.add_error('password', '비밀번호가 다릅니다.')
            else:
                self.email = user.email