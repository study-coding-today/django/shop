from django import forms
from .models import Order


class RegisterForm(forms.Form):
    quantity = forms.IntegerField(
        error_messages={
            'required': '수량을 입력하세요!'
        }, label='수량'
    )
    product = forms.IntegerField(
        error_messages={
            'required': '상품 입력하세요!'
        }, label='상품가격', widget=forms.HiddenInput
    )

    def clean(self):
        # 템플릿에서 받아온 데이터를 딕셔너리로 할당
        cleaned_data = super().clean()
        # 딕셔너리 데이터를 키별로 할당한다
