from django import forms
from .models import Product


class RegisterForm(forms.Form):
    name = forms.CharField(
        error_messages={
            'required': '상품명을 입력하세요!'
        }, max_length=64, label='상품명'
    )
    price = forms.IntegerField(
        error_messages={
            'required': '가격을 입력하세요!'
        }, label='상품가격'
    )
    description = forms.CharField(
        error_messages={
            'required': '상품내용을 입력하세요!'
        }, label='상품설명'
    )
    stock = forms.IntegerField(
        error_messages={
            'required': '재고를 입력하세요!'
        }, label='재고'
    )

    def clean(self):
        # 템플릿에서 받아온 데이터를 딕셔너리로 할당
        cleaned_data = super().clean()
        # 딕셔너리 데이터를 키별로 할당한다
        name = cleaned_data.get('name')
        price = cleaned_data.get('price')
        description = cleaned_data.get('description')
        stock = cleaned_data.get('stock')

        if name and price and description and stock:
            product = Product(
                name=name,
                price=price,
                description=description,
                stock=stock
            )
            # db에 저장하기
            product.save()